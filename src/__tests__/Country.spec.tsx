import React from 'react';
import {render, act, wait, RenderResult, screen} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import {loader} from 'graphql.macro';
import {MemoryRouter, Router, Route} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import Country from '../pages/Country';

const GetCountry = loader('../graphql/GetCountry.gql');

const mockedHistoryPush = jest.fn();

jest.mock('react-router-dom', () => {
    return {
        ...jest.requireActual('react-router-dom'),
        useHistory: () => ({
           push: mockedHistoryPush
        }),
        Link: ({children} : {children: React.ReactNode}) => children,
    };
});

describe('Country Page',() => {
  it('Should be able render country page from dashboard page',async () => {
    const mocks = [
      {
        request: {
          query: GetCountry,
          variables: {
            search: 'Brazil'
          },
        },
        result: {
          data: {
            Country: [
                {
                    name:'Brazil',
                    capital:'Brasília',
                    flag:{svgFile:'svgFile'},
                    population: 200,
                    area:200,
                    topLevelDomains:[
                        {
                            name:"name",
                            countries:[
                                {name:'name'}
                            ]
                        }
                    ]
                } 
            ],
          },
        },
      },
    ];

    const history = createMemoryHistory()
    history.push('/countries/Brazil/');

    let component: RenderResult = render(<p></p>);
    await act(async ()=>{
      component = render(
        <Router  history={history}>
            <MockedProvider mocks={mocks} addTypename={false}>
                <Route exact path='/countries/:country+' component={Country} />
            </MockedProvider>
        </Router>
      ); 
      await wait();
    });
    
    await wait();

    expect(component.getByTestId('country-info')).toBeInTheDocument()
  });

  it('Should show error UI', async () => {
    const mock = {
      request: {
        query: GetCountry,
        variables: { serach: 'Brazil' },
      },
      error: new Error('Connection Fail.'),
    };
  
    const history = createMemoryHistory()
    history.push('/countries/Brazil/');

    let component: RenderResult = render(<p></p>);
    await act(async ()=>{
      component = render(
        <Router  history={history}>
            <MockedProvider mocks={[mock]} addTypename={false}>
                <Route exact path='/countries/:country+' component={Country} />
            </MockedProvider>
        </Router>
      ); 
      await wait();
    });
  
    await new Promise(resolve => setTimeout(resolve, 0)); // wait for response
  
    const errorContent = component.getByTestId('error-message');
    expect(errorContent).toBeInTheDocument();
  });  
});