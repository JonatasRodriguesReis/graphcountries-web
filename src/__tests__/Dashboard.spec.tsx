import React, { Children } from 'react';
import {render, act, wait, RenderResult, fireEvent} from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import {loader} from 'graphql.macro';
import DashBoard from '../pages/Dashboard';
import renderer from 'react-test-renderer';

const LoadCountries = loader('../graphql/LoadCountries.gql');

jest.mock('react-router-dom', () => {
    return {
        useHistory: jest.fn(),
        Link: ({children} : {children: React.ReactNode}) => children,
    };
});

describe('Dashboard Page',() => {
  it('Should be able to render countries',async () => {
    const mocks = [
      {
        request: {
          query: LoadCountries,
          variables: {
            search: ''
          },
        },
        result: {
          data: {
            Country: [
              {name:'Brazil',capital:'Brasília',flag:{svgFile:'svgFile'} }, 
              {name:'Argentina',capital:'Buenos Aires',flag:{svgFile:'svgFile'} }
            ],
          },
        },
      },
    ];

    let component: RenderResult | undefined;
    await act(async ()=>{
      component = render(
        <MockedProvider mocks={mocks} addTypename={false}>
          <DashBoard/>
        </MockedProvider>
      ); 
      await wait();
    });
    
    const countryElement = component?.getByTestId('Brazil');
    expect(countryElement).toBeInTheDocument();
  });

  it('Should show error UI', async () => {
    const mock = {
      request: {
        query: LoadCountries,
        variables: { name: 'Buck' },
      },
      error: new Error('Connection Fail.'),
    };
  
    let component: RenderResult | undefined;
    await act(async ()=>{
      component = render(
        <MockedProvider mocks={[mock]} addTypename={false}>
          <DashBoard/>
        </MockedProvider>
      ); 
      await wait();
    });
  
    await new Promise(resolve => setTimeout(resolve, 0)); // wait for response
  
    const elem = component?.getByTestId('error-message');
    expect(elem).toBeInTheDocument();
  });  

  it('Should be able to search a country',async () => {
    const mocks = [
      {
        request: {
          query: LoadCountries,
          variables: {
            search: 'Brazil'
          },
        },
        result: {
          data: {
            Country: [
              {name:'Brazil',capital:'Brasília',flag:{svgFile:'svgFile'} }, 
              //{name:'Argentina',capital:'Buenos Aires',flag:{svgFile:'svgFile'} }
            ],
          },
        },
      },
    ];

    let component: RenderResult = render(<p></p>);
    act(async ()=>{
      component = render(
        <MockedProvider mocks={mocks} addTypename={false}>
          <DashBoard/>
        </MockedProvider>
      ); 
      await wait();
    });

    const searchField = component.getByPlaceholderText('Digite o nome do país');
    const buttonSearch = component.getByText('Pesquisar');

    fireEvent.change(searchField, {target: {value: 'Brazil'}});
    fireEvent.click(buttonSearch);

    await wait();

    const countryElement = component.getByTestId('Brazil');
    expect(countryElement).toBeInTheDocument();
  });

  it('Should not be able to search a country with a empty word',async () => {
    const mocks = [
      {
        request: {
          query: LoadCountries,
          variables: {
            search: ''
          },
        },
        result: {
          data: {
            Country: [
              {name:'Brazil',capital:'Brasília',flag:{svgFile:'svgFile'} }, 
            ],
          },
        },
      },
    ];

    let component: RenderResult = render(<p></p>);
    act(async ()=>{
      component = render(
        <MockedProvider mocks={mocks} addTypename={false}>
          <DashBoard/>
        </MockedProvider>
      ); 
      await wait();
    });

    const searchField = component.getByPlaceholderText('Digite o nome do país');
    const buttonSearch = component.getByText('Pesquisar');

    fireEvent.change(searchField, {target: {value: ''}});
    fireEvent.click(buttonSearch);

    await wait();

    const countryElement = component.getByTestId('error-search');
    expect(countryElement).toBeInTheDocument();
  });
});