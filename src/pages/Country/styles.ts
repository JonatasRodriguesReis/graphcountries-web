import styled from 'styled-components';

export const Header = styled.header`
    display: flex;
    align-items: center;
    justify-content: space-between;

    a {
        display: flex;
        align-items: center;
        text-decoration: none;
        color: #a8a8b3;
        transition: color 0.2s;

        &:hover{
            color: #666;
        }
    }
  
`;

export const Issues = styled.div`
    margin-top: 80px;
    max-width: 700px;

    a {
        background: #FFFFFF;
        border-radius: 5px;
        width: 100%;
        padding: 24px;
        display: block;
        text-decoration: none;
        transition: transform 0.2s;

        display: flex;
        align-items:center;

        &:hover {
            transform: translateX(10px);
        }

        & + a {
            margin-top: 16px;
        }

        div {
            margin: 0px 16px;
            flex: 1;

            strong {
                font-size: 20px;
                color: #3d3d4d;
            }

            p {
                font-size: 18px;
                color: #a8a8b3;
                margin-top: 4px;
            }
        }
 
        svg {
            margin-left: auto;
            color: #cbcbd6;
        }
    }
`;