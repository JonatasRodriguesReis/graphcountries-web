import React, {useState, useEffect} from 'react';
import {useRouteMatch, Link} from 'react-router-dom';
import {Header} from './styles';
import { FiChevronLeft , FiChevronRight} from 'react-icons/fi';
import CountryDetails from '../../components/Country/CountryDetails';

interface CountryParams{
    country: string;
};

interface TopLevelDomains{
    name: string;
}

interface Country{
    name: string;
    capital: string;
    flag: {
        svgFile: string;
    };
    population: Number;
    area: Number;
    topLevelDomains: TopLevelDomains[];
}

const Country: React.FC = () => {
    const {params} = useRouteMatch<CountryParams>();

    return  <>
                <Header>
                    <Link to="/">
                        <FiChevronLeft size={16} />
                        Voltar
                    </Link>
                </Header>
                <CountryDetails countryName={params.country}/>
            </>
}

export default Country;