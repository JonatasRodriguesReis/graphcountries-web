import React, {useState, useEffect ,FormEvent} from 'react';
import { Title, Form, Error} from './styles';
import ListCountries from '../../components/Dashboard/DashBoardCountries';

interface Country{
    name: string;
    capital: string;
    flag: {
        svg: string;
    };
}

const Dashboard: React.FC= ()=>{
    const [searchCountry, setSearchCountry] = useState('');
    /* const [countries, setCountries] = useState<Country[]>(()=>{
        const storageCountries = localStorage.getItem('@GraphCountry:countries');

        if(storageCountries)
            return JSON.parse(storageCountries);
        return []
    }); */
    const [inputError,setInputError] = useState('');

    async function handleAddCountry(event: FormEvent<HTMLFormElement>): Promise<void>{
        event.preventDefault();

        if(!searchCountry){
            setInputError('Necessário digitar o nome do país!');
            return
        }
        
        setInputError('');
    }

    return (
        <>
            <Title id="testT">Explore países ao redor do mundo</Title>

            <Form hasError={!!inputError} onSubmit={handleAddCountry}>
                <input type="text" value={searchCountry} onChange={(e) => setSearchCountry(e.target.value)} placeholder="Digite o nome do país"></input>
                <button type="submit">Pesquisar</button>
            </Form>

            { inputError && <Error data-testid="error-search">{inputError}</Error> }

            <ListCountries search={searchCountry}/>
        </>
    );
}

export default Dashboard;