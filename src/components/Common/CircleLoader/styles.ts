import styled from 'styled-components';

export const Content = styled.div`
    display: flex;
    max-width: 700px;
    margin-top: 8px;
    justify-content: center;
`;

export const CircleLoader = styled.div`
    border: 3px solid #fff;
    border-radius: 50%;
    border-top: 3px solid #c6c6c6;
    width: 60px;
    height: 60px;
    animation: spin 1s linear infinite;
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
`;