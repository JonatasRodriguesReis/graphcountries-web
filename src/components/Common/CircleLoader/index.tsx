import React from 'react';
import {Content, CircleLoader} from './styles'; 

const CircleLoaderContent: React.FC= ()=>{

    return (
        <Content>
            <CircleLoader/>
        </Content>
    );
}

export default CircleLoaderContent;