import styled from 'styled-components';

export const Error = styled.div`
    display: flex;
    align-items: center;
    color: #fff;
    max-width: 700px;
    height: 60px;
    border-radius: 10px;
    background-color: #F55656;
    margin-top: 8px;
    justify-content: center;
    padding: 8px 16px;
`;