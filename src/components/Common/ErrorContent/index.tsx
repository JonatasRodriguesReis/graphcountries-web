import React from 'react';
import {Error} from './styles'; 

interface Properties{
    message: string;
}

const ErrorContent: React.FC<Properties>= ({children, ...properties})=>{

    return (
        <Error data-testid="error-message">
            Houve algo errado! {properties.message} Por favor, tente novamente.
        </Error>
    );
}

export default ErrorContent;