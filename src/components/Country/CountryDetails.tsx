import React from 'react';
import {loader} from 'graphql.macro';
import {CountryInfo} from './styles';
import { useQuery } from '@apollo/client';
import CircleLoaderContent from '../Common/CircleLoader'; 
import ErrorContent from '../Common/ErrorContent';
const GetCountry = loader('../../graphql/GetCountry.gql');

interface CountryParams{
    country: string;
};

interface TopLevelDomains{
    name: string;
}

interface Country{
    name: string;
    capital: string;
    flag: {
        svgFile: string;
    };
    population: Number;
    area: Number;
    topLevelDomains: TopLevelDomains[];
}

interface Properties{
    countryName: string;
}

const CountryDetails: React.FC<Properties> = ({children, ...properties}) => {
    const { loading, error, data , fetchMore} = useQuery(
        GetCountry,
        {
            variables:{
                search:properties.countryName
            }
        }
    );

    if (loading) return <CircleLoaderContent/>;
    if (error) return <ErrorContent message={error.message}/>;

    const countryData: Country = data.Country[0];

    return  <>
                {
                    countryData && (
                        <CountryInfo data-testid="country-info">
                            <header>
                                <img src={countryData.flag.svgFile} alt={countryData.name} />
                                <div>
                                    <strong>{countryData.name}</strong>
                                    <p>{countryData.capital}</p>
                                </div>
                            </header>
                            <ul>
                                <li>
                                    <strong>{countryData.population}</strong>
                                    <span>População</span>
                                </li>
                                <li>
                                    <strong>{countryData.area}</strong>
                                    <span>Área</span>
                                </li>
                                <li>
                                    <strong>{countryData.topLevelDomains[0].name}</strong>
                                    <span>Top level domain</span>
                                </li>
                            </ul>
                        </CountryInfo>
                    )
                }
            </>
}

export default CountryDetails;