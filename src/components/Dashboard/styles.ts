import styled from 'styled-components';

export const Content = styled.div`
    margin-top: 80px;
`;

export const Countries = styled.div`
max-width: 700px;

div{
    a {
        background: #FFFFFF;
        border-radius: 5px;
        width: 100%;
        padding: 24px;
        display: block;
        text-decoration: none;
        transition: transform 0.2s;

        display: flex;
        align-items:center;

        &:hover {
            transform: translateX(10px);
        }

        img {
            width: 64px;
            height: 64px;
            border-radius: 50%;
        }

        div {
            margin: 0px 16px;
            flex: 1;

            strong {
                font-size: 20px;
                color: #3d3d4d;
            }

            p {
                font-size: 18px;
                color: #a8a8b3;
                margin-top: 4px;
            }
        }

        svg {
            margin-left: auto;
            color: #cbcbd6;
        }
    }

    & + div {
        margin-top: 16px;
    }
}
`;