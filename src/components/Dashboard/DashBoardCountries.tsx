import React, {useState, useEffect ,FormEvent} from 'react';
import { Countries, Content} from './styles';
import { Link } from 'react-router-dom';
import { FiChevronRight } from 'react-icons/fi';
import {useQuery} from '@apollo/client';
import { loader } from 'graphql.macro';
import CircleLoaderContent from '../Common/CircleLoader'; 
import ErrorContent from '../Common/ErrorContent';
import {useHistory} from 'react-router-dom';
const LoadCountries = loader('../../graphql/LoadCountries.gql');

interface Country{
    name: string;
    capital: string;
    flag: {
        svgFile: string;
    };
}

interface Properties{
    search: string;
}

const DashBoardCountries: React.FC<Properties>= ({children, ...properties})=>{

    const { loading, error, data , fetchMore} = useQuery(
        LoadCountries,
        {
            variables:{
                search:properties.search
            }
        }
    );

    if (loading) return <Content><CircleLoaderContent/></Content>;
    if (error) return <Content><ErrorContent message={error.message}/></Content>;
    return (
        <Content>
            <Countries data-testid="countries">
                {data && data.Country.map((country: Country) => (
                    <div data-testid={country.name} key={country.name} >
                        <Link to={`/countries/${country.name}`}>
                            <img
                                src={country.flag.svgFile}
                                alt={country.name}
                            />

                            <div>
                                <strong>{country.name}</strong>
                                <p>{country.capital}</p>
                            </div>

                            <FiChevronRight size={20}></FiChevronRight>
                        </Link>
                    </div>
                ))}
            </Countries>

            {/* {data.Country && data.Country.hasMore && (
                <button
                    onClick={async () => {
                    await fetchMore({
                        variables: {
                        after: data.launches.cursor,
                        },
                    });
                    }}
                >
                    Load More
                </button>
            )} */}
        </Content>
    );
}

export default DashBoardCountries;