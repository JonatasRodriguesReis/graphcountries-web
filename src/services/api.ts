import { ApolloClient, InMemoryCache, Reference} from '@apollo/client';

const client = new ApolloClient({
  uri: 'https://graphcountriesapi2.netlify.app/.netlify/functions/graphql',
  cache: new InMemoryCache({
    /* typePolicies: {
        Query: {
          fields: {
            Country: {
              keyArgs: false,
              merge(existing, incoming) {
                let Country: Reference[] = [];
                console.log(incoming)
                console.log(existing)
                if (existing && existing.Country) {
                  Country = Country.concat(existing.Country);
                }
                if (incoming && incoming.Country) {
                  Country = Country.concat(incoming.Country);
                }
                return {
                  ...incoming,
                   Country
                };
              }
            }
          }
        }
      }
    */
    }),
  defaultOptions:{
    query: {
        fetchPolicy: 'network-only',
        errorPolicy: 'all',
      },
  }
});

export default client;