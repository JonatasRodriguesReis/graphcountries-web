import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {ApolloProvider} from '@apollo/client';
import GlobalStyle from './styles/global';
import Routes from './routes';
import client from './services/api';

const App: React.FC = () => 
  <ApolloProvider client={client}>
    <BrowserRouter> 
      <Routes />
    </BrowserRouter>
    <GlobalStyle/>
  </ApolloProvider>

export default App;
