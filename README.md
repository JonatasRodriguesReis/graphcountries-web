# GraphCountries - Web

<h1 align="center">
    <img alt="GraphCountries" title="#GraphCountries" src="./src/assets/banner.png" />
</h1>

Este projeto faz parte do Desafio Técnico Front-End Pleno/Sênior aplicada pela Datum.

## 🚀 Descrição

Uma aplicação web foi desenvolvida utilizando React para consumir a API Graph Countries
(https://github.com/lennertVanSever/graphcountries) conforme as especificações apresentadas a seguir.

## Funcionalidades

[X] Criar uma lista de cards para exibir os países mostrando a bandeira, o nome e a capital dele;
<br/>
[X] Possibilitar o usuário buscar países;
<br/>
[X] Na lista, permitir que o usuário possa ir para a página de detalhes do país e ver uma lista mais completa de informações (bandeira, nome, capital, área, população e top-level domain);
<br/>
[ ]  Criar um formulário para editar os dados de um país (salvando apenas no client-side);

## Restrições técnicas
[X] Utilizar o create-react-app como base;
<br/>
[ ]  Utilizar redux para gerenciar o estado;
<br/>
[X] Utilizar react-router para trocar de página;
<br/>
[X] Utilizar @testing-library/react para testes;

## Diferenciais:
[X] Criar uma pipeline no GitLab; (Exemplo: build => test => deploy);
<br/>
[X] Entregar o projeto publicado e funcionando em alguma URL;
<br/>
[X] Garanta 100% de cobertura no projeto com testes unitários;
<br/>
[ ]  Substituir o redux pelo Local state management do Apollo Client;

## 🔧 Instalação

### `npm install`

É responsável por instalar todas as dependências do projeto

### `npm start`

É responsável por executar a aplicação em modo de desenvolvimento

### `npm test`

É responsável por executar os testes unitários da aplicação

### `npm build`

É responsável por fazer o build da aplicação e prepará-la para o modo de produção

## Resultados

![Farmers Market Finder Demo](./src/assets/apresentacao-web.gif)


## ⚙️ Executando os testes

Conforme o especificado, os teste unitários foram implementados com o objetivo de cobrir 100% da aplicação. Na imagem abaixo, é possível perceber que o objetivo dos testes unitários foi alcançado.

<h1 align="center">
    <img alt="GraphCountries" title="#GraphCountries" src="./src/assets/coverage.png" />
</h1>

A página responsável por mostrar a cobertura dos testes, pode ser obtida como um aterfato do stage de teste da pipeline.

## Pipeline criada no GitLab: build => test => deploy

Foi desenvolvido também uma pipeline no gitlab para organizar três stages da aplicação: O build, test e o deploy. As imagens abaixo mostram o funcionamento da pipeline.

<h1 align="center">
    <img alt="GraphCountries" title="#GraphCountries" src="./src/assets/pipeline1.png" />
</h1>

<h1 align="center">
    <img alt="GraphCountries" title="#GraphCountries" src="./src/assets/pipeline2.png" />
</h1>

## Deploy da aplicação com uma url de acesso público

O deploy é realizado após o stage de teste ser executado corretamente. Para isso, foi utilizado o Netlify como serviço de hospedagem das aplicação na nuvem.

URL da Aplicação Web: https://graphcountries.netlify.app/



